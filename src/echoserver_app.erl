%%%-------------------------------------------------------------------
%% @doc echoserver public API
%% @end
%%%-------------------------------------------------------------------

-module(echoserver_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    echoserver_sup:start_link(),
    start_socket().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
start_socket() ->
    case gen_tcp:listen(8091, [{active,true}, binary]) of
        {ok, LSocket} ->
            case gen_tcp:accept(LSocket) of
                {ok, Socket} ->
                    supervisor:start_child(#{id => echoserver, start => {echoserver, start_link, [Socket]},
                                             restart => permanent, type => worker, modules => [echoserver]});
                {error, Error} ->
                    io:format("accept socket error ~p~n", Error)
            end;
        {error, Error} ->
            io:format("listen socket error ~p~n", Error)
    end.
